package io.dee.note.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import io.dee.note.data.db.daos.noteDataDao.NoteDataDao
import io.dee.note.data.model.NoteData

@Database(
    entities = [NoteData::class],
    version = 3
)
abstract class AppDataBase : RoomDatabase() {
    abstract val noteDao: NoteDataDao

}