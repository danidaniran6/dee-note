package io.dee.note.data.db

import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

val Migration_1_2 = object : Migration(1, 2) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("Create table NoteData (id Integer primary key Not Null , title Text Not Null, body Text Not Null, date Text Not Null)")
    }
}
val Migration_2_3 = object : Migration(1, 2) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("ALTER TABLE NoteData ADD COLUMN  isEdited integer not null DEFAULT 0 ")
        database.execSQL("ALTER TABLE NoteData ADD COLUMN isPinned integer not null DEFAULT 0 ")
    }
}