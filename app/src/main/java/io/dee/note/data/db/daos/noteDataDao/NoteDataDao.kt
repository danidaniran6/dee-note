package io.dee.note.data.db.daos.noteDataDao

import androidx.lifecycle.LiveData
import androidx.room.*
import io.dee.note.data.model.NoteData

@Dao
interface NoteDataDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertNote(noteData: NoteData)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertNotes(noteData: List<NoteData>)


    @Delete
    suspend fun deleteNote(noteData: NoteData)

    @Delete
    suspend fun deleteNote(noteData: List<NoteData>)


    @Query("select * from NoteData order by id desc")
    suspend fun getAllNotes(): List<NoteData>

    @Query("select * from NoteData where id =:id")
    suspend fun getNote(id: Int): NoteData

    @Query("select * from NoteData order by id desc")
    fun getAllNotesLive(): LiveData<List<NoteData>>

    @Transaction
    suspend fun editNote(note: NoteData) {
        val prevNote: NoteData? = getNote(note.id)
        prevNote?.let { prvNote ->
            deleteNote(prvNote)
        }
        insertNote(note)
    }

    @Transaction
    suspend fun updateNotificationPinStatus(id: Int) {
        val note: NoteData? = getNote(id)
        note?.let { it ->
            it.isPinned = false
            insertNote(it)
        }
    }

    @Transaction
    suspend fun deleteNote(id: Int) {
        val note: NoteData? = getNote(id)
        note?.let { it ->
            deleteNote(it)
        }
    }

}