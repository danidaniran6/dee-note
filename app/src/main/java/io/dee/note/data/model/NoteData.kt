package io.dee.note.data.model

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity
@Parcelize
data class NoteData(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    var title: String = "",
    var body: String = "",
    var date: String = "",
    @ColumnInfo(defaultValue = "0")
    var isEdited: Boolean = false,
    @ColumnInfo(defaultValue = "0")
    var isPinned: Boolean = false
) : Parcelable