package io.dee.note.data.repository

import io.dee.note.data.db.daos.noteDataDao.NoteDataDao
import io.dee.note.data.model.NoteData
import javax.inject.Inject

class NoteRepository
@Inject
constructor(
    private val noteDataDao: NoteDataDao
) {

    suspend fun getAllNotes() = noteDataDao.getAllNotes()
     fun getAllNotesLive() = noteDataDao.getAllNotesLive()
    suspend fun insertNote(data: NoteData) = noteDataDao.insertNote(data)
    suspend fun deleteNote(data: NoteData) = noteDataDao.deleteNote(data)
    suspend fun editNote(data: NoteData) = noteDataDao.editNote(data)
    suspend fun updateNotificationPinStatus(noteId:Int) = noteDataDao.updateNotificationPinStatus(noteId)
    suspend fun deleteNote(noteId:Int) = noteDataDao.deleteNote(noteId)
}