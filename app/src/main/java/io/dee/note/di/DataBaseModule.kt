package io.dee.note.di

import android.content.Context
import androidx.room.Room
import io.dee.note.data.db.AppDataBase
import io.dee.note.data.db.Migration_1_2
import io.dee.note.data.db.Migration_2_3
import io.dee.note.data.db.daos.noteDataDao.NoteDataDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DataBaseModule {
    @Singleton
    @Provides
    fun provideDataBase(@ApplicationContext context: Context): AppDataBase {
        return Room.databaseBuilder(
            context,
            AppDataBase::class.java,
            "NoteDataBase"
        ).addMigrations(
            Migration_1_2,
            Migration_2_3
        ).fallbackToDestructiveMigration().build()
    }

    @Singleton
    @Provides
    fun provideNoteDao(appDb: AppDataBase): NoteDataDao {
        return appDb.noteDao
    }
}