package io.dee.note.di

import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

/*    @Singleton
    @Provides
    fun provideNoteRepository(noteDataDao: NoteDataDao): NoteRepository {
        return NoteRepository(
            noteDataDao
        )
    }*/
}