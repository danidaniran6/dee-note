package io.dee.note

import android.app.Application
import io.dee.note.util.NotificationUtil
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MainApp : Application() {

    override fun onCreate() {
        super.onCreate()
        NotificationUtil.createChannel(
            this,
            getString(R.string.app_name),
            getString(R.string.channel_Id)
        )
    }
}