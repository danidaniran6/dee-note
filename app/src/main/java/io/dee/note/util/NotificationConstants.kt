package io.dee.note.util

class NotificationConstants {
    companion object {
        const val DEFAULT_GROUP_CHANNEL_NAME = "NOTE GROUP CHANNEL"
        const val DEFAULT_GROUP_CHANNEL_ID = "DEFAULT_GROUP_CHANNEL_ID"

        const val ACTION_UNPIN_NOTIFICATION = "ACTION_UNPIN_NOTIFICATION"
            const val ACTION_DELETE_NOTIFICATION = "ACTION_DELETE_NOTIFICATION"
            const val ACTION_EDIT_NOTIFICATION = "ACTION_EDIT_NOTIFICATION"
        const val OPEN_APP_NOTIFICATION = "OPEN_APP_NOTIFICATION"

    }
}