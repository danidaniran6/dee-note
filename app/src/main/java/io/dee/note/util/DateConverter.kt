package io.dee.note.util

import java.text.SimpleDateFormat
import java.util.*

fun getDate(format: String, locale: Locale = Locale.getDefault()): String {
    val date = Calendar.getInstance().time
    val formatter = SimpleDateFormat(format, locale)
    return formatter.format(date)
}