package io.dee.note.util

import android.Manifest
import android.app.*
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationCompat.EXTRA_NOTIFICATION_ID
import androidx.core.app.NotificationManagerCompat
import io.dee.note.R
import io.dee.note.data.model.NoteData
import io.dee.note.services.NotificationIntentService
import io.dee.note.ui.MainActivity
import io.dee.note.ui.WelcomeActivity
import io.dee.note.util.NotificationConstants.Companion.ACTION_DELETE_NOTIFICATION
import io.dee.note.util.NotificationConstants.Companion.ACTION_EDIT_NOTIFICATION
import io.dee.note.util.NotificationConstants.Companion.ACTION_UNPIN_NOTIFICATION
import io.dee.note.util.NotificationConstants.Companion.DEFAULT_GROUP_CHANNEL_ID
import io.dee.note.util.NotificationConstants.Companion.DEFAULT_GROUP_CHANNEL_NAME
import io.dee.note.util.NotificationConstants.Companion.OPEN_APP_NOTIFICATION
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
object NotificationUtil {
    private var channelId: String? = ""
    private var groupChannelId: String? = ""


    @RequiresApi(Build.VERSION_CODES.O)
    fun createDefaultGroup(context: Context) {
        val groupChannel = NotificationChannelGroup(
            DEFAULT_GROUP_CHANNEL_ID,
            DEFAULT_GROUP_CHANNEL_NAME
        )
        val manager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        manager.createNotificationChannelGroup(groupChannel)
    }

    fun createChannel(context: Context, channelName: String, channelId: String) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createDefaultGroup(context)
            NotificationUtil.channelId = channelId
            val channel = NotificationChannel(
                channelId,
                channelName,
                NotificationManager.IMPORTANCE_DEFAULT
            ).apply {
                lightColor = Color.BLUE
                enableLights(true)
                enableVibration(true)
                group = DEFAULT_GROUP_CHANNEL_ID
            }

            val manager =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            manager.createNotificationChannel(channel)
        }
    }


    fun createNotification(context: Context, noteData: NoteData): Notification? {
        val disableNotificationPinAction: PendingIntent? =
            createPinPendingIntent(context, noteData)
        val deleteNoteIntent: PendingIntent? =
            deleteNoteIntent(context, noteData)
        val openAppAction: PendingIntent? = createOpenAppIntent(context, noteData)
        val editNoteIntent: PendingIntent? = editNoteIntent(context, noteData)
        val notification = NotificationCompat.Builder(context, channelId!!).apply {
            setContentTitle(noteData.title)
                .setContentText(noteData.body)
                .setSmallIcon(R.drawable.ic_notification_icon)
                .setAutoCancel(false)
                .setDefaults(Notification.DEFAULT_VIBRATE)
                .setOngoing(true)
                .addAction(
                    R.drawable.ic_round_notifications_none_24,
                    context.getString(R.string.unPin_Notification),
                    disableNotificationPinAction
                )
                .addAction(
                    R.drawable.ic_round_delete_24,
                    context.getString(R.string.delete_Notification),
                    deleteNoteIntent
                )
                .addAction(
                    R.drawable.ic_baseline_edit_24,
                    context.getString(R.string.edit_Notification),
                    editNoteIntent
                )
                .setShowWhen(true)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setColorized(true)
                .setContentIntent(openAppAction)
                .setStyle(NotificationCompat.InboxStyle())
                .setGroup(groupChannelId).priority = NotificationCompat.PRIORITY_HIGH

        }.build()
        val manager =
            NotificationManagerCompat.from(context)
        notification.flags = NotificationCompat.FLAG_NO_CLEAR
        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.POST_NOTIFICATIONS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return null
        }
        manager.notify(noteData.id, notification)
        return notification
    }

    private fun createPinPendingIntent(context: Context, noteData: NoteData): PendingIntent? {
        val unpinIntent = Intent(context, NotificationIntentService::class.java).apply {
            action = ACTION_UNPIN_NOTIFICATION
            putExtra(EXTRA_NOTIFICATION_ID, noteData.id)
        }
        return PendingIntent.getBroadcast(context, 0, unpinIntent, PendingIntent.FLAG_IMMUTABLE)
    }

    private fun createOpenAppIntent(context: Context, noteData: NoteData): PendingIntent? {
        val intent = Intent(context, WelcomeActivity::class.java).apply {
            action = OPEN_APP_NOTIFICATION
        }
        return PendingIntent.getActivity(context, 267, intent, PendingIntent.FLAG_IMMUTABLE)
    }

    private fun editNoteIntent(context: Context, noteData: NoteData): PendingIntent? {
        val intent = Intent(context, MainActivity::class.java).apply {
            action = ACTION_EDIT_NOTIFICATION
            putExtra(ACTION_EDIT_NOTIFICATION, noteData)
        }
        return PendingIntent.getActivity(context, 267, intent, PendingIntent.FLAG_IMMUTABLE)
    }

    private fun deleteNoteIntent(context: Context, noteData: NoteData): PendingIntent? {
        val unpinIntent = Intent(context, NotificationIntentService::class.java).apply {
            action = ACTION_DELETE_NOTIFICATION
            putExtra(EXTRA_NOTIFICATION_ID, noteData.id)
        }
        return PendingIntent.getBroadcast(context, 0, unpinIntent, PendingIntent.FLAG_IMMUTABLE)
    }

    fun discardNotification(context: Context, noteData: NoteData) {
        val manager =
            NotificationManagerCompat.from(context)
        manager.cancel(noteData.id)
    }

    fun discardNotification(context: Context, id: Int) {
        val manager =
            NotificationManagerCompat.from(context)
        manager.cancel(id)
    }

}