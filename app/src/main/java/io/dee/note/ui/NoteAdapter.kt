package io.dee.note.ui

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import io.dee.note.R

import io.dee.note.data.model.NoteData
import io.dee.note.databinding.NoteItemViewBinding

class NoteAdapter(
    val event: ((event: NoteItemEvent<NoteData>) -> Unit) = {}
) : RecyclerView.Adapter<NoteAdapter.ViewHolder>() {

    inner class ViewHolder(private val binding: NoteItemViewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun onBind(note: NoteData) {
            binding.apply {
                titleText.text = note.title
                bodyText.text = note.body
                tvDate.text = note.date
                ivDelete.setOnClickListener {
                    event?.invoke(NoteItemEvent.DeleteNote(note))
                }
                ivEdit.setOnClickListener {
                    event?.invoke(NoteItemEvent.EditNote(note))
                }


                val noneNotif: Drawable? = ContextCompat.getDrawable(
                    itemView.context,
                    R.drawable.ic_round_notifications_none_24
                )
                val activeNotif: Drawable? = ContextCompat.getDrawable(
                    itemView.context,
                    R.drawable.ic_baseline_notifications_active_24
                )
                ivPinToNotification.setImageDrawable(
                    if (note.isPinned)
                        activeNotif
                    else
                        noneNotif
                )
                ivPinToNotification.setOnClickListener {
                    when (note.isPinned) {
                        false -> {
                            ivPinToNotification.setImageDrawable(
                                ContextCompat.getDrawable(
                                    itemView.context,
                                    R.drawable.ic_baseline_notifications_active_24
                                )
                            )
                            event?.invoke(NoteItemEvent.PinToNotification(note, true))
                        }

                        true -> {
                            ivPinToNotification.setImageDrawable(
                                ContextCompat.getDrawable(
                                    itemView.context,
                                    R.drawable.ic_round_notifications_none_24
                                )
                            )
                            event?.invoke(NoteItemEvent.PinToNotification(note, false))
                        }
                        else -> {

                        }

                    }
                }
            }
        }
    }


    val differ = AsyncListDiffer(this, object : DiffUtil.ItemCallback<NoteData>() {
        override fun areItemsTheSame(oldItem: NoteData, newItem: NoteData): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: NoteData, newItem: NoteData): Boolean {
            return oldItem == newItem
        }
    })

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = NoteItemViewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = differ.currentList[position]
        holder.onBind(item)
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }
}


sealed class NoteItemEvent<T> {
    object None : NoteItemEvent<Nothing>()
    data class DeleteNote(val note: NoteData) : NoteItemEvent<NoteData>()
    data class EditNote(val note: NoteData) : NoteItemEvent<NoteData>()
    data class ShareNote(val note: NoteData) : NoteItemEvent<NoteData>()
    data class PinToNotification(val note: NoteData, val toPin: Boolean) : NoteItemEvent<NoteData>()
}