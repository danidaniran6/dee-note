package io.dee.note.ui

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import io.dee.note.data.model.NoteData
import io.dee.note.util.NotificationConstants
import io.dee.note.util.NotificationUtil
import io.dee.note.util.getDate
import dagger.hilt.android.AndroidEntryPoint
import io.dee.note.R
import io.dee.note.databinding.ActivityMainBinding

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val viewModel: NoteViewModel by viewModels()
    private var toEditNote: NoteData? = null
    private var noteAdapter = NoteAdapter(
        event = { event ->
            when (event) {
                is NoteItemEvent.DeleteNote -> {
                    viewModel.setState(MainStateEvent.DeleteNote(event.note))
                    NotificationUtil.discardNotification(this, event.note)
                    closeTransition()
                }
                is NoteItemEvent.EditNote -> {
                    toEditNote = event.note
                    toEditNote?.let {
                        startTransition()
                        editNoteProperties(it)
                    }
                }
                is NoteItemEvent.PinToNotification -> {
                    when (event.toPin) {
                        true -> {
/*                            val intent = Intent(this, NotificationService::class.java).apply {
                                putExtra("noteData", event.note)
                                putExtra("state", true)
                            }
                            startService(intent)*/
                            NotificationUtil.createNotification(this@MainActivity, event.note)
                        }
                        false -> {
/*                            val intent = Intent(this, NotificationService::class.java).apply {
                                putExtra("noteData", event.note)
                                putExtra("state", false)
                            }
                            stopService(intent)*/
                            NotificationUtil.discardNotification(this@MainActivity, event.note)
                        }
                    }
                    viewModel.setState(MainStateEvent.SetNote(event.note.apply {
                        isPinned = event.toPin
                    }
                    ))
                }
                is NoteItemEvent.ShareNote -> {}
                else -> {}
            }
        }
    )

    private fun editNoteProperties(note: NoteData) {
        binding.apply {
            etTitle.setText(note.title)
            etBody.setText(note.body)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        checkIntents()
        binding.apply {
            rcView.apply {
                adapter = noteAdapter
                layoutManager = LinearLayoutManager(this@MainActivity)
            }
            etTitle.setOnFocusChangeListener { view, b ->
                if (b) {
                    startTransition()
                }
            }

            btnAddNote.setOnClickListener {
                val note: NoteData =
                    if (toEditNote != null) toEditNote!!.apply {
                        title = etTitle.text.toString()
                        body = etBody.text.toString()
                        isEdited = true
                        /*        viewModel.setState(MainStateEvent.EditNote(this))*/
                        if (toEditNote?.isPinned == true) {
                            NotificationUtil.createNotification(this@MainActivity, this)
                        }
                    } else NoteData().apply {
                        title = etTitle.text.toString()
                        body = etBody.text.toString()
                        date = getDate("dd/MM/yyyy")
/*                        viewModel.setState(MainStateEvent.SetNote(this))*/
                    }
                viewModel.setState(MainStateEvent.SetNote(note))
                toEditNote = null

                closeTransition()
            }
            btnDismiss.setOnClickListener {
                closeTransition()
            }

        }

        subscribeObserves()
        viewModel.setState(MainStateEvent.GetAllNotes)

    }

    private fun checkIntents() {
        intent?.let {
            when (
                intent.action
            ) {
                NotificationConstants.ACTION_EDIT_NOTIFICATION -> {
                    val note =
                        intent.getParcelableExtra<NoteData>(NotificationConstants.ACTION_EDIT_NOTIFICATION)
                    note?.let {
                        toEditNote = it
                        startTransition()
                        editNoteProperties(it)
                    }
                }
                else -> {}
            }
        }
    }

    private fun startTransition() {
        binding.apply {
            if (cntAddNote.progress == 0.0f) {
                cntAddNote.transitionToEnd()
                etTitle.hint = getString(R.string.your_Note_title)
            }
        }
    }

    private fun closeTransition() {
        binding.apply {
            cntAddNote.transitionToStart()
            etTitle.hint = getString(R.string.create_note)
            etBody.text.clear()
            etTitle.text.clear()
            etTitle.clearFocus()
            etBody.clearFocus()
//            noteAdapter.notifyDataSetChanged()
        }
    }

    private fun subscribeObserves() {
        viewModel.notes.observe(this,
            { noteData ->
                noteAdapter.differ.submitList(noteData)
            })
    }

    override fun onBackPressed() {
        binding.apply {
            if (cntAddNote.progress == 1.0f) {
                closeTransition()
            } else {
                super.onBackPressed()
            }
        }
    }

}