package io.dee.note.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.assisted.Assisted
import dagger.hilt.android.lifecycle.HiltViewModel
import io.dee.note.data.model.NoteData
import io.dee.note.data.repository.NoteRepository
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NoteViewModel
@Inject
constructor(
    private val noteRepository: NoteRepository) : ViewModel() {

    private val _notes: MutableLiveData<List<NoteData>> = MutableLiveData()
    val notes: LiveData<List<NoteData>>
        get() = _notes


    fun setState(mainStateEvent: MainStateEvent<NoteData>) {
        viewModelScope.launch {
            when (mainStateEvent) {
                is MainStateEvent.GetAllNotes -> {
                    delay(20)
                    _notes.postValue(noteRepository.getAllNotes())
                }

                is MainStateEvent.GetTodayNotes -> {}
                is MainStateEvent.SetNote -> {
                    mainStateEvent.data?.let {
                        noteRepository.insertNote(it)
                        delay(20)
                    }
                }

                is MainStateEvent.DeleteNote -> {
                    mainStateEvent.data?.let {
                        noteRepository.deleteNote(it)
                    }
                }

                is MainStateEvent.EditNote -> {
                    mainStateEvent.data?.let {
                        noteRepository.editNote(it)
                    }
                }

                is MainStateEvent.None -> {}
            }
            setState(MainStateEvent.GetAllNotes)
        }
    }

}


sealed class MainStateEvent<out T> {
    object None : MainStateEvent<Nothing>()
    object GetAllNotes : MainStateEvent<Nothing>()
    object GetTodayNotes : MainStateEvent<Nothing>()
    data class SetNote(val data: NoteData) : MainStateEvent<NoteData>()
    data class DeleteNote(val data: NoteData) : MainStateEvent<NoteData>()
    data class EditNote(val data: NoteData) : MainStateEvent<NoteData>()
}