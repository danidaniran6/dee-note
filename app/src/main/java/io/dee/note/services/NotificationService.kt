package io.dee.note.services

import android.app.Service
import android.content.Intent
import android.os.IBinder
import io.dee.note.data.model.NoteData
import io.dee.note.util.NotificationUtil
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class NotificationService :
    Service() {
    override fun onBind(intent: Intent?): IBinder? {

        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        intent?.let {
            val note = intent?.getParcelableExtra<NoteData>("noteData")
            note?.let {
                val notification = NotificationUtil.createNotification(
                    applicationContext,
                    it
                )
                notification?.let { startForeground(note.id, notification) }

            }
        }

        super.onStartCommand(intent, flags, startId)
        return START_STICKY
    }

    override fun onCreate() {
        super.onCreate()
    }

    override fun onDestroy() {

        super.onDestroy()
    }
}