package io.dee.note.services

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import io.dee.note.data.repository.NoteRepository
import io.dee.note.util.NotificationConstants.Companion.ACTION_DELETE_NOTIFICATION
import io.dee.note.util.NotificationConstants.Companion.ACTION_UNPIN_NOTIFICATION
import io.dee.note.util.NotificationUtil
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class NotificationIntentService() : BroadcastReceiver() {
    @Inject
    lateinit var repository: NoteRepository
    override fun onReceive(context: Context?, intent: Intent?) {
        context?.let { cxt ->
            intent?.let {
                when (it.action) {
                    ACTION_UNPIN_NOTIFICATION -> {
                        val noteId: Int =
                            intent.getIntExtra(NotificationCompat.EXTRA_NOTIFICATION_ID, -1)
                        if (noteId != -1) {
                            GlobalScope.launch(Dispatchers.IO) {
                                repository.updateNotificationPinStatus(noteId)
                            }
                            NotificationUtil.discardNotification(cxt, noteId)
                        }
                    }
                    ACTION_DELETE_NOTIFICATION -> {
                        val noteId: Int =
                            intent.getIntExtra(NotificationCompat.EXTRA_NOTIFICATION_ID, -1)
                        if (noteId != -1) {
                            GlobalScope.launch(Dispatchers.IO) {
                                repository.deleteNote(noteId)
                            }
                            NotificationUtil.discardNotification(cxt, noteId)
                        }
                    }
                    else -> {}
                }
            }
        }
    }
}